//
//  APIEmpresas.h
//  Empresas
//
//  Created by João Silva on 13/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import "API.h"

@interface APIEmpresas : API

+ (NSURLSessionDataTask *)buscarEmpresaComNome:(NSString *)nome eComBlock:(APIResponseBlock)block;

@end
