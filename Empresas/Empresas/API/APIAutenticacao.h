//
//  APIAutenticacao.h
//  Empresas
//
//  Created by João Silva on 12/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import "API.h"

@interface APIAutenticacao : API

+ (NSURLSessionDataTask *)loginComUsuario:(NSString *)usuario comSenha:(NSString *)senha eComBlock:(APIResponseBlock)block;

@end
