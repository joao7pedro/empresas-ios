//
//  APIAutenticacao.m
//  Empresas
//
//  Created by João Silva on 12/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import "APIAutenticacao.h"

@interface APIAutenticacao()

@end

@implementation APIAutenticacao

+ (NSURLSessionDataTask *)loginComUsuario:(NSString *)usuario comSenha:(NSString *)senha eComBlock:(APIResponseBlock)block
{
    API *request = [API make:APIMethodPOST requestWithPath:@"users/auth/sign_in" parameters:@{@"email": usuario, @"password": senha} cacheOption:APICacheOptionBoth completion:block];
    request.shouldSaveCache = NO;
    
    return [request makeRequest];
}

@end
