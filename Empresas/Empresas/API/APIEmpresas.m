//
//  APIEmpresas.m
//  Empresas
//
//  Created by João Silva on 13/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import "APIEmpresas.h"

@implementation APIEmpresas

+ (NSURLSessionDataTask *)buscarEmpresaComNome:(NSString *)nome eComBlock:(APIResponseBlock)block
{
    API *request = [API make:APIMethodGET requestWithPath:@"enterprises" parameters:@{@"name": nome} cacheOption:APICacheOptionBoth completion:block];
    request.shouldSaveCache = NO;
    
    return [request makeRequest];
}

@end
