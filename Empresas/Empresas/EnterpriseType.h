//
//  EnterpriseType.h
//  Empresas
//
//  Created by João Silva on 14/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import "BaseModel.h"

@interface EnterpriseType : BaseModel

@property (assign, nonatomic) NSInteger uid;
@property (strong, nonatomic) NSString *enterpriseTypeName;

@end
