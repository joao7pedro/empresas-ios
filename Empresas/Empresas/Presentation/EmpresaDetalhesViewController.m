//
//  EmpresaDetalhesViewController.m
//  Empresas
//
//  Created by João Silva on 14/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import "EmpresaDetalhesViewController.h"
#import "UIColor+TIAdditions.h"

@interface EmpresaDetalhesViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageViewLogo;
@property (weak, nonatomic) IBOutlet UITextView *textViewDescription;

@end

@implementation EmpresaDetalhesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.topItem.title = @"";
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.textViewDescription.textColor = [UIColor ti_warmGreyColor];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = self.empresa.enterpriseName;
    
    self.imageViewLogo.image = [UIImage imageNamed:@"imgE1"];
    self.textViewDescription.text = self.empresa.descriptionObject;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    self.empresa = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
