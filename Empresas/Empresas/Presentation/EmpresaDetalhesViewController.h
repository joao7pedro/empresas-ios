//
//  EmpresaDetalhesViewController.h
//  Empresas
//
//  Created by João Silva on 14/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Enterprise.h"

@interface EmpresaDetalhesViewController : UIViewController

@property Enterprise *empresa;

@end
