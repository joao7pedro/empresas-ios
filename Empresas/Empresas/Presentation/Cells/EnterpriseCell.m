//
//  EnterpriseCell.m
//  Empresas
//
//  Created by João Silva on 14/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import "EnterpriseCell.h"
#import "UIColor+TIAdditions.h"

@implementation EnterpriseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.labelName.textColor = [UIColor ti_darkIndigoColor];
    self.labelType.textColor = [UIColor ti_warmGreyColor];
    self.labelLocation.textColor = [UIColor ti_warmGreyColor];
}

@end
