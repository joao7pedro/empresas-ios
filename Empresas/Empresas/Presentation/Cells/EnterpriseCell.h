//
//  EnterpriseCell.h
//  Empresas
//
//  Created by João Silva on 14/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnterpriseCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewLogo;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelType;
@property (weak, nonatomic) IBOutlet UILabel *labelLocation;

@end
