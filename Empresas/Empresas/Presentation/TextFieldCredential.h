//
//  TextFieldCredential.h
//  Empresas
//
//  Created by João Silva on 13/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldCredential : UITextField

- (void)configurarComIcone:(NSString *)icone;

@end
