//
//  EmpresasViewController.m
//  Empresas
//
//  Created by João Silva on 13/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import "EmpresasViewController.h"
#import "APIEmpresas.h"
#import "Enterprise.h"
#import "Helper.h"
#import "EnterpriseCell.h"
#import "EmpresaDetalhesViewController.h"
#import "UIColor+TIAdditions.h"

@interface EmpresasViewController () <UISearchControllerDelegate, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UISearchController *searchBarController;
@property (weak, nonatomic) IBOutlet UITableView *tableViewEmpresas;

@property (strong, nonatomic) UILabel *labelMensagem;

@property UIBarButtonItem *barButtonBusca;

@property NSArray *listaDeEmpresas;
@property Enterprise *empresaSelecionada;

@property BOOL buscando;

@end

@implementation EmpresasViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configurandoVisualInicial];
    [self configurandoBarraDeBusca];
    [self configurandoTabela];
    [self configurandoLabelMensagem];
    
    [self mostrarElementosNavigationBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configurandoVisualInicial {
    self.navigationController.navigationBar.barTintColor = [UIColor ti_darkishPinkColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.view.backgroundColor = [UIColor ti_beigeColor];
}

-(void)configurandoBarraDeBusca {
    self.barButtonBusca = self.navigationItem.rightBarButtonItem;
    
    self.searchBarController = [[UISearchController alloc]initWithSearchResultsController:nil];
    
    self.searchBarController.delegate = self;
    self.searchBarController.searchBar.delegate = self;
    
    self.searchBarController.hidesNavigationBarDuringPresentation = NO;
    self.searchBarController.dimsBackgroundDuringPresentation = NO;
    self.searchBar.showsCancelButton = YES;
}

-(void)configurandoTabela {
    self.tableViewEmpresas.delegate = self;
    self.tableViewEmpresas.dataSource = self;
    self.tableViewEmpresas.backgroundColor = [UIColor ti_beigeColor];
}

-(void)configurandoLabelMensagem {
    self.labelMensagem = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableViewEmpresas.bounds.size.width, self.tableViewEmpresas.bounds.size.height)];
    self.labelMensagem.text = @"Clique na busca para iniciar";
    self.labelMensagem.textColor = [UIColor ti_charcoalGreyColor];
    self.labelMensagem.textAlignment = NSTextAlignmentCenter;
}

-(void)mostrarElementosNavigationBar {
    UIImage *image = [UIImage imageNamed:@"logoNav"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    
    self.navigationItem.rightBarButtonItem = self.barButtonBusca;
}

-(void)ocultarElementosNaavigationBar {
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.titleView = nil;
}

- (IBAction)tocouBotaoBuscar:(id)sender {
    
    [self ocultarElementosNaavigationBar];
    self.tableViewEmpresas.backgroundView = nil;
    
    self.navigationItem.titleView = self.searchBarController.searchBar;
    self.definesPresentationContext = YES;
    
    [self.searchBarController.searchBar becomeFirstResponder];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self mostrarElementosNavigationBar];
    self.listaDeEmpresas = nil;
    self.buscando = NO;
    
    [self.tableViewEmpresas reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.buscando = YES;
    
    [APIEmpresas buscarEmpresaComNome:searchText eComBlock:^(id _Nonnull response, NSError * _Nonnull error, BOOL cache)
     {
         NSArray *empresas = [Helper transformDictionaryArray:response[@"enterprises"] intoArrayOfModels:[Enterprise class]];
         self.listaDeEmpresas = empresas;
         
         [self.tableViewEmpresas reloadData];
     }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.listaDeEmpresas.count > 0) {
        self.tableViewEmpresas.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.tableViewEmpresas.backgroundView = nil;
        return 1;
    }
    else {
        
        if(self.buscando)
            self.labelMensagem.text = @"Nenhum Resultado";
        else
            self.labelMensagem.text = @"Clique na busca para iniciar";
        
        self.tableViewEmpresas.backgroundView = self.labelMensagem;
        self.tableViewEmpresas.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listaDeEmpresas.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 113;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EnterpriseCell *celulaEmpresa = [self.tableViewEmpresas dequeueReusableCellWithIdentifier:@"enterpriseCell"];
    
    Enterprise *empresa = self.listaDeEmpresas[indexPath.row];
    
    celulaEmpresa.imageViewLogo.image = [UIImage imageNamed:@"imgE1Lista"];
    celulaEmpresa.labelName.text = empresa.enterpriseName;
    celulaEmpresa.labelType.text = empresa.enterpriseType.enterpriseTypeName;
    celulaEmpresa.labelLocation.text = [NSString stringWithFormat:@"%@ %@", empresa.city, empresa.country];
    
    return celulaEmpresa;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.destinationViewController isKindOfClass:[EmpresaDetalhesViewController class]]) {
        EmpresaDetalhesViewController *empresaDetalhesViewController = segue.destinationViewController;
        empresaDetalhesViewController.empresa = self.listaDeEmpresas[self.tableViewEmpresas.indexPathForSelectedRow.row];
        
        [self.tableViewEmpresas deselectRowAtIndexPath:self.tableViewEmpresas.indexPathForSelectedRow  animated:YES];
    }
}

@end
