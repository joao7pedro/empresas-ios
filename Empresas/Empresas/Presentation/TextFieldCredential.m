//
//  TextFieldCredential.m
//  Empresas
//
//  Created by João Silva on 13/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import "TextFieldCredential.h"
#import "UIColor+TIAdditions.h"

@interface TextFieldCredential ()

@property NSString *icone;

@end

@implementation TextFieldCredential

- (void)configurarComIcone:(NSString *)icone {
    self.icone = icone;
}

-(void)setPlaceholder:(NSString *)placeholder
{
    NSAttributedString *attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:56.0/255.0 green:55.0/255.0 blue:67.0/255.0 alpha:0.5 ] }];
    self.attributedPlaceholder = attributedPlaceholder;
}

-(void)drawRect:(CGRect)rect  {
    UIImage *icon = [UIImage imageNamed:self.icone];
    UIImageView * iconImage = [[UIImageView alloc] initWithImage:icon];
    iconImage.frame = CGRectMake(0.0, 0.0, iconImage.image.size.width + 10, iconImage.image.size.height);
    [iconImage setContentMode:UIViewContentModeLeft];
    self.leftView = iconImage;
    self.leftViewMode = UITextFieldViewModeAlways;
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.8;
    border.borderColor = [[UIColor ti_charcoalGreyColor] colorWithAlphaComponent:0.6].CGColor;
    border.frame = CGRectMake(0, self.frame.size.height - borderWidth, self.frame.size.width + iconImage.image.size.width, self.frame.size.height);
    border.borderWidth = borderWidth;
    [self.layer addSublayer:border];
    self.layer.masksToBounds = YES;
    
    [self setBackgroundColor:[UIColor clearColor]];
}

@end
