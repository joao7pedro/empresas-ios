//
//  LoginViewController.m
//  Empresas
//
//  Created by João Silva on 12/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import "LoginViewController.h"
#import "APIAutenticacao.h"
#import "Util/UIButton+TIAddictions.h"
#import "Util/UIColor+TIAdditions.h"
#import "Presentation/TextFieldCredential.h"

@interface LoginViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet TextFieldCredential *textFieldEmail;
@property (weak, nonatomic) IBOutlet TextFieldCredential *textFieldSenha;
@property (weak, nonatomic) IBOutlet UIButton *buttonEntrar;

@property (strong, nonatomic) UIActivityIndicatorView *indicadorDeAtividade;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self comportamentoVisualInicial];
    [self configurandoIndicadorDeAtividade];
    
    self.textFieldEmail.delegate = self;
    self.textFieldSenha.delegate = self;
}

-(void)configurandoIndicadorDeAtividade {
    self.indicadorDeAtividade = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicadorDeAtividade.frame = CGRectMake(0.0, 0.0, 60.0, 60.0);
    self.indicadorDeAtividade.center = self.view.center;
    [self.view addSubview:self.indicadorDeAtividade];
    [self.indicadorDeAtividade bringSubviewToFront:self.view];
}

-(void)comportamentoVisualInicial
{
    self.view.backgroundColor = [UIColor ti_beigeColor];
    
    [self.textFieldEmail setPlaceholder:@"E-mail"];
    [self.textFieldEmail configurarComIcone:@"icEmail"];
    [self.textFieldSenha setPlaceholder:@"Senha"];
    [self.textFieldSenha configurarComIcone:@"icCadeado"];
    
    [UIButton roundedButton:self.buttonEntrar];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == self.textFieldEmail)
       [self.textFieldSenha becomeFirstResponder];
    else
        [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)tocouBotaoEntrar:(id)sender {
    [self.indicadorDeAtividade startAnimating];
    
    [APIAutenticacao loginComUsuario:self.textFieldEmail.text comSenha:self.textFieldSenha.text eComBlock:^(id _Nonnull response, NSError * _Nonnull error, BOOL cache)
     {
         [self.indicadorDeAtividade stopAnimating];
         if(error == nil)
            [self performSegueWithIdentifier:@"segueLoginSuccessful" sender:self];
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

@end
