//
//  main.m
//  Empresas
//
//  Created by João Silva on 12/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
