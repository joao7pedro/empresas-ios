//
//  Enterprise.h
//  Enterprise
//
//  Created by João Silva on 13/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import "BaseModel.h"
#import "EnterpriseType.h"

@interface Enterprise : BaseModel

@property (assign, nonatomic) NSInteger uid;
@property (strong, nonatomic) NSString *enterpriseName;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) EnterpriseType *enterpriseType;
@property (strong, nonatomic) NSString *descriptionObject;

@end
