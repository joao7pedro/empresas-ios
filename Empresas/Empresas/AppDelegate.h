//
//  AppDelegate.h
//  Empresas
//
//  Created by João Silva on 12/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

