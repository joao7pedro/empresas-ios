//
//  UIButton+TIAddictions.m
//  Empresas
//
//  Created by João Silva on 12/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import "UIButton+TIAddictions.h"
#import "UIColor+TIAdditions.h"
#import "UIFont+TIAdditions.h"

@implementation UIButton (TIAdditions)

+ (void)roundedButton:(UIButton *)button {
    button.layer.cornerRadius = 4;
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor ti_greenyBlueColor].CGColor;
    
    button.backgroundColor = [UIColor ti_greenyBlueColor];
    
    button.titleLabel.font = [UIFont systemFontOfSize:24.0];
    button.tintColor = [UIColor ti_whiteColor];
}

@end
