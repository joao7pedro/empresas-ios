//
//  UIFont+TIAdditions.m
//  Teste_iOS
//
//  Generated on Zeplin. (by teste_ios on 2017-5-12).
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "UIFont+TIAdditions.h"

@implementation UIFont (TIAdditions)

+ (UIFont *)ti_textStyleFont {
    return [UIFont fontWithName:@"Roboto-Bold" size:16.0f];
}

+ (UIFont *)ti_textStyle2Font {
    return [UIFont systemFontOfSize:15.0f weight:UIFontWeightRegular];
}

@end
