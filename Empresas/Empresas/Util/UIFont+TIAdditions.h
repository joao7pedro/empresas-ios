//
//  UIFont+TIAdditions.h
//  Teste_iOS
//
//  Generated on Zeplin. (by teste_ios on 2017-5-12).
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (TIAdditions)

+ (UIFont *)ti_textStyleFont;
+ (UIFont *)ti_textStyle2Font;

@end
