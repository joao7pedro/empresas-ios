//
//  UIColor+TIAdditions.m
//  Teste_iOS
//
//  Generated on Zeplin. (by teste_ios on 2017-5-12).
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "UIColor+TIAdditions.h"

@implementation UIColor (TIAdditions)

+ (UIColor *)ti_greenyBlueColor {
    return [UIColor colorWithRed:87.0f / 255.0f green:187.0f / 255.0f blue:188.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)ti_beigeColor {
    return [UIColor colorWithRed:235.0f / 255.0f green:233.0f / 255.0f blue:215.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)ti_blackColor {
    return [UIColor colorWithWhite:3.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)ti_mediumPinkColor {
    return [UIColor colorWithRed:238.0f / 255.0f green:76.0f / 255.0f blue:119.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)ti_darkishPinkColor {
    return [UIColor colorWithRed:222.0f / 255.0f green:71.0f / 255.0f blue:114.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)ti_darkIndigoColor {
    return [UIColor colorWithRed:26.0f / 255.0f green:14.0f / 255.0f blue:73.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)ti_steelColor {
    return [UIColor colorWithRed:142.0f / 255.0f green:142.0f / 255.0f blue:147.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)ti_coolGrey60Color {
    return [UIColor colorWithRed:171.0f / 255.0f green:179.0f / 255.0f blue:189.0f / 255.0f alpha:0.6f];
}

+ (UIColor *)ti_coolGrey30Color {
    return [UIColor colorWithRed:171.0f / 255.0f green:179.0f / 255.0f blue:189.0f / 255.0f alpha:0.3f];
}

+ (UIColor *)ti_coolGrey50Color {
    return [UIColor colorWithRed:171.0f / 255.0f green:179.0f / 255.0f blue:189.0f / 255.0f alpha:0.5f];
}

+ (UIColor *)ti_black50Color {
    return [UIColor colorWithWhite:0.0f alpha:0.5f];
}

+ (UIColor *)ti_white10Color {
    return [UIColor colorWithWhite:1.0f alpha:0.1f];
}

+ (UIColor *)ti_softGreenColor {
    return [UIColor colorWithRed:125.0f / 255.0f green:192.0f / 255.0f blue:117.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)ti_warmGreyColor {
    return [UIColor colorWithRed:141.0f / 255.0f green:140.0f / 255.0f blue:140.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)ti_whiteColor {
    return [UIColor colorWithWhite:1.0f alpha:1.0f];
}

+ (UIColor *)ti_charcoalGreyColor {
    return [UIColor colorWithRed:56.0f / 255.0f green:55.0f / 255.0f blue:67.0f / 255.0f alpha:1.0f];
}

@end
