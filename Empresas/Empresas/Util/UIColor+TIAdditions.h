//
//  UIColor+TIAdditions.h
//  Teste_iOS
//
//  Generated on Zeplin. (by teste_ios on 2017-5-12).
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (TIAdditions)

+ (UIColor *)ti_greenyBlueColor;
+ (UIColor *)ti_beigeColor;
+ (UIColor *)ti_blackColor;
+ (UIColor *)ti_mediumPinkColor;
+ (UIColor *)ti_darkishPinkColor;
+ (UIColor *)ti_darkIndigoColor;
+ (UIColor *)ti_steelColor;
+ (UIColor *)ti_coolGrey60Color;
+ (UIColor *)ti_coolGrey30Color;
+ (UIColor *)ti_coolGrey50Color;
+ (UIColor *)ti_black50Color;
+ (UIColor *)ti_white10Color;
+ (UIColor *)ti_softGreenColor;
+ (UIColor *)ti_warmGreyColor;
+ (UIColor *)ti_whiteColor;
+ (UIColor *)ti_charcoalGreyColor;

@end
