//
//  UIButton+TIAddictions.h
//  Empresas
//
//  Created by João Silva on 12/05/17.
//  Copyright © 2017 João Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UIButton (TIAdditions)

+ (void)roundedButton:(UIButton *)button;

@end
